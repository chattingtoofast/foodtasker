from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Restaurant(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='restaurant')
    title = models.CharField(max_length=64)
    description = models.CharField(max_length=256)
    adress = models.CharField(max_length=64)
    logo = models.ImageField(upload_to='restaurant_logos/', blank=False)

    def __str__(self):
        return self.title


class Customer(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='customer')
    avatar = models.CharField(max_length=100)
    phone = models.CharField(max_length=100, blank=True)
    adress = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.user.get_full_name()


class Driver(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='driver')
    avatar = models.CharField(max_length=100)
    phone = models.CharField(max_length=100, blank=True)
    adress = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.user.get_full_name()
