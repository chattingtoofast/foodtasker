from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from foodtaskerapp import views as foodtaskerapp_views
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    url(r'^$', foodtaskerapp_views.homepage),
    url(r'^restaurant/$', foodtaskerapp_views.restaurant_home, name='restaurant-home'),
    url(r'^restaurant/sign-in/$', auth_views.LoginView.as_view(
        template_name='restaurant/sign_in.html', success_url='/'), name='restaurant-sign-in'),
    url(r'^restaurant/sign-up/$', foodtaskerapp_views.restaurant_sign_up,
        name='restaurant-sign-up'),
    url(r'^restaurant/sign-out/$',
        auth_views.LogoutView.as_view(next_page='/'), name='restaurant-sign-out')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
